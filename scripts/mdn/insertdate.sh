HTMLFILE=$1
MDFILE=${1%.*}.md

# Get list of html removed files from commit sha also you might want to check with A (created files) and moved ones
#git show --diff-filter=D --summary fe1962ce20b240f81367f2ae2d26edf4d47439e3

# Get date
DATE=$(git log -- $HTMLFILE | grep "Date:" | head -n 2 | tail -n 1 | awk '{print $2" "$3" "$4}' | xargs -I {} date -d"{}" +%Y%m%d)
echo $DATE

# Insert date
sed --in-place -z s/---/"htmlDate: $DATE\\n---"/2 $MDFILE
