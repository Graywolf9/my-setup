# Markdown conversion
#
# Please see https://pad.mozilla.org/p/mdn-markdown-hack-day for details.

# Look for html files in folders
for x in $(ls -d ./*/); do N=$(find ./$x -name "*.html" | wc -l); [ $N -eq 0 ] || echo -e $x":\t"$N; done

# Clean html

# JS Script
# Tabla de dos columnas format
# var f1 = document.querySelectorAll(".tableau td:nth-child(1)");
# var f2 = document.querySelectorAll(".tableau td:nth-child(2)");
# var text = "<dl>";
# f1.forEach(function(ix){
#   text += "<dt>"+it.innerHTML+"</dt><dd>"+f2[ix].innerHTML+"</dd>";
#   })
#   text += "</dl>";
#   document.querySelector("#rrr").innerHTML = text;

## Clean css classes

$FOLDER=web/http

grep -rl ' class="learn-box standard-table"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="learn-box standard-table"//g' {}
grep -rl ' class="learn-box"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="learn-box"//g' {}
grep -rl ' class="blockIndicator"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="blockIndicator"//g' {}
grep -rl ' class="hidden"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="hidden"//g' {}
grep -rl 'h3 class="brush: js"' $FOLDER | xargs -I {} sed -i '' -e 's/h3 class="brush: js"//g' {}
grep -rl ' class="Documentation"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="Documentation"//g' {}
grep -rl ' class="hps atn"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="hps atn"//g' {}
grep -rl ' class="atn hps"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="atn hps"//g' {}
grep -rl ' class="plain"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="plain"//g' {}
grep -rl ' class="newpage"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="newpage"//g' {}
grep -rl ' class="card-grid"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="card-grid"//g' {}
grep -rl ' class="syntaxbox notranslate"' $FOLDER | xargs -I {} sed -i '' -e 's/ class="syntaxbox notranslate"//g' {}

# OR // Mejorando

$CLASSES="
standard-table
learn-box
blockIndicator
hidden
brush:
Documentation
hps
atn
plain
newpage
card-grid
syntaxbox
html
editIcon
geckoVersionNote
elt
"

for x in $(echo $CLASSES); do grep -rlE "( class=\".*)${x}(.*\")" $FOLDER | xargs -I {} sed -i '' -Ee s/"( class=\".*)${x}(.*\")"/"\1\2"/g {}; done

# replace table span
grep -rlE "(<span style=\"color\: green\">)(.*)(<\/span>)" $FOLDER | xargs -I {} sed -i '' -Ee s/"(<span style=\"color\: green\">)(.*)(<\/span>)"/"\2"/g {}

# From markdown repo
# Find html first 99 files sorted and see errors for conversion details
find ../translated-content/files/es/web/css -name "*.html" | sort | head -n 99 | sed s=../translated-content/files/es/==g | xargs -I {} yarn h2m {} --locale es --mode dry

# OR

yarn h2m $FOLDER --locale es --mode dry

# Repeat until no errors

# Commit html cleanup

git add $FOLDER
git commit -m "$FOLDER html cleanup"

# Convert html to md
# From markdown repo

yarn h2m $FOLDER --locale es --mode replace

# Commit conversion

git commit -m "$FOLDER rename html to md"
git add $FOLDER
git commit -m "$FOLDER h2m replace"

# Linting md
# https://github.com/mdn/translated-content/pull/8869
# Using the config from #8181 (and deleting the current .markdownlint.json, then running markdownlint-cli2-fix "files/es/**/*.md" for these files.

npx markdownlint-cli2-fix $FOLDER/*.md
npx markdownlint-cli2-fix $FOLDER/**/*.md

# Commit linting

git add $FOLDER
git commit -m "$FOLDER linting with 8181 .markdownlint-cli2.jsonc"

# Drop the URL title attributes
# Usually do a find/replace for \]\(([^)]*) "[^"]*"\) to ]($1) to drop the title attributes that have been cleaned out upstream
# grep -rE '\]\(([^)]*) "[^"]*"\)'
# sed -i '' -Ee s/'\]\(([^)]*) "[^"]*"\)'/'](\1)'/g

grep -rlE '\]\(([^)]*) "[^"]*"\)' $FOLDER | xargs -I {} sed -i '' -Ee s/'\]\(([^)]*) "[^"]*"\)'/'](\1)'/g {}

# Commit it

git add $FOLDER
git commit -m "$FOLDER drop the url title attributes"

# Notes:
# yarn h2m is a markdown command
# npx markdownlint-cli2-fix is a content command
# https://github.com/mdn/translated-content/pull/8181
