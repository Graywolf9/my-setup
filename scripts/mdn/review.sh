i=1;
t=$(cat $1 | wc -l);
for x in $(cat $1);  do
    PR="https://pr$2.content.dev.mdn.mozit.cloud$x"
    MDN="https://developer.mozilla.org$x"
    echo "Reviewing: $PR";
    echo "MDN: $MDN";
    echo "$i/$t";
    [ `uname` = Darwin ] && open -a "firefox nightly" $PR $MDN || firefox "$MDN" & epiphany "$PR" &
    read -p "Press Enter to continue";
    i=`expr $i + 1`;
done
echo "Done!";
