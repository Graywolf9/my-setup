# Macro removal

M=gecko_minversion_header
grep -ilr $M . | for x in $(xargs); do codium $x; done
grep -ilr $M . | for x in $(xargs); do echo $x | sed s="\.\/"="http://localhost:5042/es/docs/"=g | sed s="\/index.md"==g | xargs -I {} open -a "firefox nightly" {}; done

sed -Ee s/'{{\s?spec\(("[^"]*"),\s?("[^"]*"),\s?"[^"]*"\)\s?}}'/'[\2](\1)'/g ./conflicting/web/api/eventtarget/addeventlistener/index.md

# Replace event macro with rendered url
rg -li '\{\{\s?event\("([^"]*)"\)\s?\}\}' . | xargs -I {} sed -i '' -Ee s/'\{\{\s?event\("([^"]*)"\)\s?\}\}'/'[`\1`](\/es\/docs\/Web\/Reference\/Events\/\1)'/g {}
# single quote
rg -li '\{\{\s?event\(\x27([^"]*)\x27\)\s?\}\}' . | xargs -I {} gsed -i -E s/'\{\{\s?event\(\x27([^"]*)\x27\)\s?\}\}'/'[`\1`](\/es\/docs\/Web\/Reference\/Events\/\1)'/g {}
# two arguments testing
rg -il '\{\{\s?event\("([^"]*)",\s?"([^"]*)"\)\s?\}\}' . | xargs -I {} sed -i '' -Ee s/'\{\{\s?event\("([^"]*)",\s?"([^"]*)"\)\s?\}\}'/'[`\2`](\/es\/docs\/Web\/Reference\/Events\/\1)'/g {}

# Replace interwiki with rendered url
rg -i '\{\{\s?Interwiki\("([^"]*)",\s?"([^"]*)"\)\}\}' .
gsed -E s/'\{\{\s?Interwiki\("([^"]*)",\s?"([^"]*)"\)\}\}'/'[\2](https:\/\/es.wikipedia.org\/wiki\/\2)'/ig
for x in $(ls); do gawk 'match($0(.*)\{\{\s?[Ii]nterwiki\("([^"]*)",\s?"([^"]*)"\)\}\}(.*)/){ $OR=a[3]; gsub(" ","_"[3]); $0=a[1]"["$OR"](https://es.wikipedia.org/wiki/"a[3]")"a[4] } 1' $x > $x.tmp && mv $x.tmp $x; done

rg -i '\{\{\s?Interwiki\("([^"]*)",\s?"([^"]*)",\s?"([^"]*)"\)\}\}' .
gawk 'match($0(.*)\{\{\s?[Ii]nterwiki\("([^"]*)",\s?"([^"]*)",\s?"([^"]*)"\)\}\}(.*)/){ $OR=a[3]; gsub(" ","_"[3]); $0=a[1]"["a[4]"](https://es.wikipedia.org/wiki/"a[3]")"a[5] } 1' $x > $x.tmp && mv $x.tmp $x

# Replace spec2 macro
# Replace Specifications section with their macro
perl -0pe 's/## Specificaciones[^"]*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' web/javascript/reference/global_objects/number/isfinite/index.md

for x in $(cat ~/Desktop/spec2.txt.9); do rg -il "spec2" $x; done
rg -il "spec2" . | grep -v -Ff ~/Desktop/spec2.txt | grep -v -Ff ~/Desktop/spec2.txt.2 | grep -v -Ff ~/Desktop/spec2.txt.3 | grep -v -Ff ~/Desktop/spec2.txt.4 | grep -v -Ff ~/Desktop/spec2.txt.5 | grep -v -Ff ~/Desktop/spec2.txt.6 | grep -v -Ff ~/Desktop/spec2.txt.7 | grep -v -Ff ~/Desktop/spec2.txt.8 | head -n 100 > ~/Desktop/spec2.txt.4

for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Specificaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Specifications(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Specification(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Especificaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Especificación(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Espesificaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Espicificaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Específicaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Especificiaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Especificacion(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Especificaciónes(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Espeficicaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
for x in $(cat ~/Desktop/spec2.txt.9); do perl -0pe 's/## Espedicicaciones(\s\S*?)*##/## Especificaciones\n\n{{Specifications}}\n\n##/g' $x > $x.tmp; mv $x.tmp $x; done
