#!/bin/env ruby

require 'yaml'
require 'json'

removed = Hash.new { |hash, key| hash[key] = 0 }

ARGV.each do |base|
  Dir.glob(["**/*.html", "**/*.md"], base: base).each do |path|
    file = File.join(base, path)
    next if not FileTest.file?(file)

    content = File.read(file)
    yaml = YAML.load(content)
    yaml.reject! do |key, value|
      if (key != "title" && key != "slug" && key != "original_slug" && !key.start_with?("l10n") && !key.start_with?("i10n")) 
        removed[key] += 1
        true
      else
        false
      end
    end

    new_content = YAML.dump(yaml)
    new_content += "---\n"
    
    if content.include?("title: '")
      new_content.gsub!(/title: "([^"]+)"/, "title: '\\1'")
    elsif not content.include?('title: "')
      new_content.gsub!(/title: "([^"]+)"/, "title: \\1")
    end

    in_meta = true
    content.each_line.with_index do |line, index|
      new_content += line if not in_meta

      if in_meta && line.start_with?("---") && index > 0
        in_meta = false
      end
    end

    File.write(file, new_content)
  end
end

puts JSON.pretty_generate(removed)
