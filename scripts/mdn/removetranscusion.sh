# Remove transclusion from macros

# Show ocurrences
# With "
rg -i "EmbedLiveSample *\([^,]*,[^,]*,[^,]*,[^,]*, *'[^'][^']*'"
# With '
rg -i 'EmbedLiveSample *\([^,]*,[^,]*,[^,]*,[^,]*, *"[^"][^"]*"'
