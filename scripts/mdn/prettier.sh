# Prettier

# Check
#npx prettier -c dir/to
git ls-files -mo --exclude-standard | xargs -I {} npx prettier -c {}

# Fix
#npx prettier -w dir/to`

echo "Run the next command if there is something to fix:"
echo "git ls-files -mo --exclude-standard | xargs -I {} npx prettier -w {}"
