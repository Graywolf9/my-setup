# Add sourceCommit
#node ../liondocs-cli/liondocs.cli.js --sha files/es/web/api/element/animationend_event/index.md --content ../content --translated ./ --lang es
git ls-files -mo --exclude-standard | xargs -I {} node ../liondocs-cli/liondocs.cli.js --sha {} --content ../content --translated ./ --lang es
