FILE=$1

DATE=$(grep "htmlDate" $FILE | awk '{print $2}' | xargs -I {} date -d"{}" +%Y-%m-%d)
echo $DATE

sed --in-place s/htmlDate.*/"htmlDate: $DATE"/ $FILE
