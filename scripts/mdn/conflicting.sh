# For conflicting pages: https://gist.github.com/cw118/00424f18db4476e8b76dd31842ed9bad
# Revisar slugs para ver su origen y redirección en `es` y `en-US`
# Si se eliminó en `en-US` rastrear archivo con 
git log --diff-filter=D --summary

# En `yari` corregir la redirección según lo analizado anteriormente, como debió haber sido sin el conflicto
yarn tool add-redirect /es/docs/foo /es/docs/foo2

# Eliminar y redirigir el slug con conlflicto
yarn tool delete conflicting/foo2 es --redirect foo2
yarn content delete conflicting/web/api/canvasrenderingcontext2d es --redirect web/api/canvasrenderingcontext2d

# El archivo eliminado ya estará en `commit` hacer commit a `_redirects.txt` y `_wikihistory.json`

# Move from content repo
yarn content move orphaned/Web/Guide/Events/Orientation_and_motion_data_explained Web/Events/Orientation_and_motion_data_explained es

# for orphaned, simple cases are "the page does not exist anymore anywhere in English" and must be deleted (potentially redirected to a "nice" alternative)
