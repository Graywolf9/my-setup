# Linting
# in translated-content: `yarn install`

# for check linting
#yarn lint:md

# for fix linting
#yarn fix:md 

# OR

# Check
#npx markdownlint-cli2 <direccion-al-archivo-a-analizar>
git ls-files -mo --exclude-standard | xargs -I {} npx markdownlint-cli2 {}

# Fix
#npx markdownlint-cli2 --fix <direccion-al-archivo-a-corregir>
