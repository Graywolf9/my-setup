# Clean OSX files
find . -iname "._*" -exec rm {} \;
find . -iname ".DS_Store" -exec rm {} \;

# Clean Windows files
find . -iname "Thumbs.db" -exec rm {} \;

# Clean emacs tmp files
find . -iname "*~" -exec rm {} \;
