FOR /F %%F IN ('dir /B /AD') DO (
  echo %%F
  cd %%F
  dir /b *.mp4
  FOR /F "tokens=*" %%G IN ('dir /b *.mp4') DO ffmpeg -hwaccel cuda -hwaccel_output_format cuda -i "%%G" -vf scale_cuda=1280:720 -an -c:v hevc_nvenc -metadata comment="Made with makevideo2store_na_cuda" -b:v 168K "%%~nG_lower.mp4"
  cd ..
)
