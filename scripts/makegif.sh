gTS=$(date +%s)

ffmpeg -y -i $1 -vf palettegen palette_$gTS.png
ffmpeg -y -i $1 -i palette_$gTS.png -filter_complex paletteuse -r 10 "${1%.*}.gif"

rm palette_$gTS.png
