SIZE=$(exiftool "$1" | grep "Image Size" | awk -F': ' '{ print $2 }')
ROTATION=$(exiftool "$1" | grep "Rotation" | awk -F': ' '{ print $2 }')
ROTATION=$([ $ROTATION -gt 0 ] && echo "Yes" || echo "No")
WIDTH=$(echo $SIZE | awk -Fx '{ print $1 }')
HEIGHT=$(echo $SIZE | awk -Fx '{ print $2 }')

echo "Original size: $SIZE"
echo "Rotate: $ROTATION"

ORIENTATION=$( [ $HEIGHT -gt $WIDTH ] && echo "V" || echo "H" )
echo $ORIENTATION

ORIENTATION=$( [ $ROTATION == "Yes" ] && [ $HEIGHT -lt $WIDTH ] && echo "V" || echo "H" )
echo $ORIENTATION
if [ $ROTATION == "Yes" ]; then
  HEIGHT=$(echo $SIZE | awk -Fx '{ print $1 }')
  WIDTH=$(echo $SIZE | awk -Fx '{ print $2 }')
fi
echo "$WIDTH""x""$HEIGHT"

if [ $ORIENTATION == "V" ]; then
  SOPTS=$( [ $ORIENTATION == "V" ] && [ $HEIGHT -gt 720 ] && echo "-filter:v scale=720:-1" )
else
  SOPTS=$( [ $ORIENTATION == "H" ] && [ $WIDTH -gt 1280 ] && echo "-s 1280x720" )
fi
echo $SOPTS

AUDIO=$( [ $2 ] && [ $2 == "-na" ] && echo "-an" || echo "-c:a libmp3lame -b:a 96k" )

CUDA="-hwaccel cuda -hwaccel_output_format cuda"
CUDA_C="hevc_nvenc"
CUDA_SIZE="-vf scale_cuda=1280:720"
CUDA_B="-b:v 168K"

# VAAPI AMF (AMD)
VAAPI="-hwaccel vaapi -hwaccel_output_format vaapi"
VAAPI_C="hevc_vaapi"
VAAPI_SIZE="-vf scale_vaapi=1280:720"

ffmpeg \
  -i "$1" \
  -color_primaries bt709 -color_trc bt709 -colorspace bt709 \
  -c:v libx265 \
  $SOPTS \
  -r 24 \
  $AUDIO \
  -metadata comment="Made with makevideo2store" \
  -threads 1 \
  "${1%.*}_lower.mp4"

# Ejemplos de comandos para ver el mejor. R: El más sencillo o con -r 24 es el mejor
#ffmpeg -i "%%G" -c copy -an "%%~nG_lower.mp4"
#ffmpeg -i "%%G" -c:v libx265 -an "%%~nG_lower.mp4"
#ffmpeg -i "%%G" -c:v libx265 -crf 28 -an "%%~nG_lower.mp4"
#ffmpeg -i "%%G" -c:v libx265 -b:v 166k -x275-paramas pass=1 -an -f null /dev/null && \
#  ffmpeg -i "%%G" -c:v libx265 -b:v 166k -x265-params pass=2 -an "%%~nG_lower.mp4"
#
#ffmpeg -i "%%G" -c:v libx265 -an "%%~nG_lower.mp4"
#
#ffmpeg -y -i "%%G" -c:v libx265 -b:v 2600k -x265-params pass=1 -an -f null /dev/null && \
#  ffmpeg -i "%%G" -c:v libx265 -b:v 2600k -x265-params pass=2 -c:a aac -b:a 128k "%%~nG_lower.mp4"

# Notas
# CRF: Constant Rate Factor
# - 23 default
# - lower number hight quality
# - two-pass
#    - bitrate = file size / duration
#      ejemplo: (200 MiB * 8388.608 [converts MiB to kBit; note: not 8192 as 1 kBit is always 1000 bit]) / 600 seconds = ~2796 kBit/s total bitrate
#      2796 - 128 kBit/s (desired audio bitrate) = 2668 kBit/s video bitrate

#https://trac.ffmpeg.org/wiki/Encode/H.265#Losslessencoding

# Ejemplos con cuda/vaapi
#ffmpeg -hwaccel cuda -hwaccel_output_format cuda -i "%%G" -vf scale_cuda=1280:720 -an -c:v hevc_nvenc -metadata comment="Made with makevideo2store_na_cuda" -b:v 168K "%%~nG_lower.mp4"

#makevideo2store_na.sh
# TODO: autodetectar orientación
# TODO: detectar si es una resolución menor y conservar esa
# TODO: Test oga
#ffmpeg -i "$1" -c:v libx265 -s 1280x720 -r 24 -an -metadata comment="Made with makevideo2store_na.sh" -threads 2 "${1%.*}_lower.mp4"

#makevideo2store_wa.sh
#TODO: Sync with na
#ffmpeg -i "$1" -c:v libx265 -s 1280x720 -r 24 -c:a libmp3lame -b:a 96k -metadata comment="Made with makevideo2store_wa.sh" -threads 1 "${1%.*}_lower.mp4"

#makevideo2store_wa_v.sh
#ffmpeg -i "$1" -c:v libx265 -filter:v scale=720:-1 -r 24 -c:a libmp3lame -b:a 96k -metadata comment="Made with makevideo2store_wa_v.sh" -threads 1  "${1%.*}_lower.mp4"
