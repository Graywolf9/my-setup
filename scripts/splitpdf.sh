IFS=$'\n'
GWDIR=$(echo $1 | cut -d. -f1)
mkdir $GWDIR
pdftk $1 burst output $GWDIR/${1%.*}_%04d.pdf
rm $GWDIR/doc_data.txt
