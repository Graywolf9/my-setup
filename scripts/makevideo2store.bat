FOR /F %%F IN ('dir /B /AD') DO (
  echo %%F
  cd %%F
  dir /b *.mp4
  FOR /F "tokens=*" %%G IN ('dir /b *.mp4') DO ffmpeg -i "%%G" -c:v libx265 -s 1280x720 -an -metadata comment="Make with makevideo2store" "%%~nG_lower.mp4"
  cd ..
)
