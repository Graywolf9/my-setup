IFS=$'\n';
for x in $(ls *.mp3 | sort -h); do
  echo $x;
  A=`exiftool $x | grep ^Artist | awk -F: '{ print $2 }'`;
  T=`exiftool $x | grep ^Title | awk -F: '{ print $2 }'`;

  N="${x%.*}";
  [ $(echo $A | xargs 2>/dev/null) ] && N="$N - $A";
  [ $(echo $T | xargs 2>/dev/null) ] && N="$N - $T";
  mv "$x" "$N.mp3"
done
