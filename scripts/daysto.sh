# Get day of the year (only works in linux)
#date -d"2022-12-17" +%j

D1=2023-03-31
D2=2023-07-19
D3=2023-12-15
TD=$(date +%Y-%m-%d)

DD=$D3

[ $(uname) == "Darwin" ] &&
  echo $( expr $(date -jf "%Y-%m-%d" $DD "+%j") - $(date +%j) ) ||
  echo $( expr $(date -d$DD +%j) - $(date +%j) )


# Vacaciones abril
[ $(date +%j) -lt 101 ] && echo $( expr 101 - $(date +%j) ) | figlet && exit
# Vacaciones julio
[ $(date +%j) -lt 201 ] && echo $( expr 201 - $(date +%j) ) | figlet && exit
# Vacaciones diciembre
[ $(date +%j) -lt 351 ] && echo $( expr 351 - $(date +%j) ) | figlet
