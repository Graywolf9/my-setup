#if [ "$(command -v wget)" != ""  ]; then
#    wget tlamaki.mx/utilidades/gwvimrc.tar;
#else
#    if [ "$(command -v curl)" != "" ]; then
#        curl -O tlamaki.mx/utilidades/gwvimrc.tar;
#    else
#        echo "You need wget or curl command";
#        exit 1;
#    fi
#fi

mkdir ~/.vim_runtime
tar -xf config/gwvimrc.tar -C ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh
echo "set -g mouse on" >> ~/.tmux.conf
echo "
    \" GrayWolf
    nnoremap <silent> <F3> :NERDTreeToggle<CR>
    nnoremap <silent> <S-F> :NERDTreeToggle<CR>
    \"inoremap <silent> <C-F> <Esc>:NERDTreeToggle<CR>
    :set mouse=a
    let g:NERDTreeMouseMode=3
    let g:NERDTreeWinPos = \"left\"
    let g:user_emmet_leader_key=','
    :set nu
" >> ~/.vimrc
