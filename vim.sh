#/bin/bash

if [ "$(command -v curl)" == "" ]; then echo "curl not detected."; apt install curl; fi

echo "Getting vim-plug..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
echo "vim-plug done!"

echo "Copying config..."
cp dots/.vimrc ~/.vimrc
echo "config copyed!"

echo "Installing plugins..."
vim -c 'PlugInstall' -c 'qa!'
echo "Plugins installed!"

echo "Installing nodejs for coc..."
curl -sL install-node.now.sh/lts | bash
echo "nodejs installed!"
