Name:		dwm
Version:	6.4
Release:	1%{?dist}
Summary:	Dynamic Window Manager
License:	GPL 3.0
URL:		https://tlamaki.mx
Source:		%{name}-%{version}.tar.gz

%description
Dynamic Window Manager with dracula theme

%prep
%setup -q -n dwm

%build
make

%install
make install DESTDIR=%{buildroot}

%files
/usr/local/bin/dwm

%doc %attr(0444,root,root) /usr/local/share/man/man1/dwm.1
