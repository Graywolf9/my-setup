#git clone https://git.suckless.org/dwm
#cd dwm
#curl -O https://raw.githubusercontent.com/Graywolf9/dracula-dwm/patch-file/dracula-theme.patch
#patch -p1 < dracula-theme.patch
#sudo make clean install
#cd
#echo "exec dwm" >> .xinitrc
#echo "run: startx"


[ -d ./dwm/ ] || git clone https://git.suckless.org/dwm
cd dwm
curl -O https://raw.githubusercontent.com/Graywolf9/dracula-dwm/patch-file/dracula-theme.patch
patch -p1 < dracula-theme.patch
cd ..

tar -czf dwm-6.4.tar.gz dwm

podman run -ti --rm -v $PWD:/root/files $(podman build -q .)
