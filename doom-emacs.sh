#!/bin/bash

# Set package manager
alias lin=apt
[ "$(command -v apt)" == "" ] && alias lin=dnf

# Check if emacs is installed
[ "$(command -v emacs)" == "" ] && sudo lin install emacs

# Check dependencies for doom emacs
[ "$(command -v git)" == "" ] && sudo lin install git
[ "$(command -v rg)" == "" ] && sudo lin install ripgrep
[ "$(command -v find)" == "" ] && sudo lin install find

# Check if doom emacs is already installed
[ "$(command -v ~/.emacs.d/bin/doom)" == "" ] || exit

# Install doom emacs
[ -d ~/.emacs.d ] && mv ~/.emacs.d ~/emacs.d.b
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
~/.emacs.d/bin/doom install
