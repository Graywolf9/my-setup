(require 'evil)
(evil-mode 1)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'dracula t)

(require 'ox-latex)
(add-to-list 'org-latex-logfiles-extensions "tex")
