%define _binaries_in_noarch_packages_terminate_build   0

Summary:          Simple Terminal
License:          GPL
Name:             st
Version:          0.9
Release:          1%{?dist}
BuildArch:        noarch
Source:           %{name}-%{version}.tar.gz
Group:            Development/Tools

%description
Simple Terminal with dracula theme

%prep
%setup -q -n st

%build
make

%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
/usr/local/bin/st

%doc %attr(0444,root,root) /usr/local/share/man/man1/st.1
